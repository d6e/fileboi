extern crate checksum;
extern crate walkdir;

use checksum::crc::Crc as crc;
use std::fs;
use std::path::Path;
use walkdir::WalkDir;

fn main() {
    let root = ".";
    for entry in WalkDir::new(root) {
        let entry = entry.unwrap();
        let filepath = entry.path();
        if filepath == Path::new(".") || filepath == Path::new("..") {
            continue;
        }
        println!("Generating checksum for {}", filepath.display());
        let checksum: Result<u64, String> = filepath.to_str().map_or(
            Err(format!("Failed to get path for {}", filepath.display())),
            |s| {
                crc::new(s)
                    .checksum()
                    .map(|x| x.crc64)
                    .map_err(|x| x.to_owned())
            },
        );
        if checksum.is_ok() {
            println!("Checksum: {}", checksum.unwrap())
        } else {
            eprintln!("ERROR: {}", checksum.unwrap_err())
        }
    }
}
